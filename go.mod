module github.com/sapiderman/seed-go

go 1.15

require (
	github.com/google/martian v2.1.0+incompatible // indirect
	github.com/gorilla/mux v1.7.4
	github.com/nelkinda/health-go v0.0.1
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.1
	go.elastic.co/apm/module/apmgorilla v1.8.0
)
