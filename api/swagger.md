# API documentations

## What's this for

API documentations using OpenAPI specification. Swagger is a set of open-source tools built around the OpenAPI Specification that can help you design, build, document and test REST APIs right into your service.

## References

1. [swagger editor docker](https://hub.docker.com/r/swaggerapi/swagger-editor/)
2. [swagger-editor](https://editor.swagger.io/)
3. [OpenAPI specification](https://swagger.io/resources/open-api/)